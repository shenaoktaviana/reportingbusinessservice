﻿using BusinessServiceFramework;
using BusinessServiceFramework.Models;
using ReportingBusinessService.Models;
using ReportingDomainModel;
using ReportingDomainModel.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace ReportingBusinessService
{
    public class SettingsService : BaseService<DomainRepository<Setting>, Setting>
    {
        public SettingsService()
            : base()
        {

        }

        public SettingsService(long loginUserID)
            : base(loginUserID)
        {

        }

        public List<Setting> GetRelatedSettings(long userID)
        {
            List<Setting> settings = this.GetBy(x => x.UserID == null || x.UserID.Value == userID).ToList();

            return settings;
        }

        public BaseSettings GetSettingObj(Setting model)
        {
            BaseSettings setting = null;

            if (!string.IsNullOrEmpty(model.Value))
            {
                switch (model.Type)
                {
                    case "G":
                        setting = (GeneralSettings)JsonConvert.DeserializeObject(model.Value, typeof(GeneralSettings));
                        break;
                    case "D":
                        setting = (DeveloperSettings)JsonConvert.DeserializeObject(model.Value, typeof(DeveloperSettings));
                        break;
                    case "I":
                        setting = (Info)JsonConvert.DeserializeObject(model.Value, typeof(Info));
                        break;
                }
            }

            return setting;
        }

        public DeveloperSettings GetDeveloperSetting()
        {
            Setting setting = this.GetBy(x => x.Type == "D").FirstOrDefault();

            DeveloperSettings developerSetting = null;

            if (setting != null)
            {
                developerSetting = (DeveloperSettings)this.GetSettingObj(setting);
            }

            if (developerSetting == null)
            {
                developerSetting = new DeveloperSettings();

                //TODO:: do we have to have default value???
            }

            return developerSetting;
        }

        public void SmartInsert(DeveloperSettings model)
        {
            Setting setting = this.GetBy(x => x.Type == "D").FirstOrDefault();

            JsonSerializerSettings jsonSetting = new JsonSerializerSettings();
            jsonSetting.DefaultValueHandling = DefaultValueHandling.Ignore;
            jsonSetting.NullValueHandling = NullValueHandling.Ignore;

            if (setting != null)
            {
                setting.Value = JsonConvert.SerializeObject(model, jsonSetting);
                this.UpdateSetting(setting);
            }
            else
            {
                setting = new Setting();
                setting.Type = "D";
                setting.Value = JsonConvert.SerializeObject(model, jsonSetting);

                this.InsertSetting(setting);
            }
        }

        public Info GetInfo()
        {
            Setting setting = this.GetBy(x => x.Type == "I").FirstOrDefault();

            Info info = null;

            if (setting != null)
            {
                info = (Info)this.GetSettingObj(setting);
            }

            if (info == null)
            {
                info = new Info();

                //TODO:: do we have to have default value???
            }

            return info;
        }

        public void SmartInsert(Info model)
        {
            Setting setting = this.GetBy(x => x.Type == "I").FirstOrDefault();

            JsonSerializerSettings jsonSetting = new JsonSerializerSettings();
            jsonSetting.DefaultValueHandling = DefaultValueHandling.Ignore;
            jsonSetting.NullValueHandling = NullValueHandling.Ignore;

            if (setting != null)
            {
                setting.Value = JsonConvert.SerializeObject(model, jsonSetting);
                this.UpdateSetting(setting);
            }
            else
            {
                setting = new Setting();
                setting.Type = "I";
                setting.Value = JsonConvert.SerializeObject(model, jsonSetting);

                this.InsertSetting(setting);
            }
        }

        public GeneralSettings GetGeneralSetting()
        {
            Setting setting = this.GetBy(x => x.Type == "G").FirstOrDefault();

            GeneralSettings generalSetting = null;

            if (setting != null)
            {
                generalSetting = (GeneralSettings)this.GetSettingObj(setting);
            }

            if (generalSetting == null)
            {
                generalSetting = new GeneralSettings();

                //TODO:: do we have to have default value???
            }

            return generalSetting;
        }

        public void SmartInsert(GeneralSettings model)
        {
            Setting setting = this.GetBy(x => x.Type == "G").FirstOrDefault();

            JsonSerializerSettings jsonSetting = new JsonSerializerSettings();
            jsonSetting.DefaultValueHandling = DefaultValueHandling.Ignore;
            jsonSetting.NullValueHandling = NullValueHandling.Ignore;

            if (setting != null)
            {
                setting.Value = JsonConvert.SerializeObject(model, jsonSetting);
                this.UpdateSetting(setting);
            }
            else
            {
                setting = new Setting();
                setting.Type = "G";
                setting.Value = JsonConvert.SerializeObject(model, jsonSetting);

                this.InsertSetting(setting);
            }
        }

        private Setting UpdateSetting(Setting model)
        {
            TransactionOptions transOpt = new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.RequiresNew, transOpt))
            {
                try
                {
                    this.Update(model);

                    this.Save();

                    transaction.Complete();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return model;
        }

        private Setting InsertSetting(Setting model)
        {
            TransactionOptions transOpt = new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.RequiresNew, transOpt))
            {
                try
                {
                    this.Insert(model);

                    this.Save();

                    transaction.Complete();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            return model;
        }

        public void DeleteSetting(Setting model)
        {
            TransactionOptions transOpt = new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.RequiresNew, transOpt))
            {
                try
                {
                    this.Delete(model);

                    this.Save();

                    transaction.Complete();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

    }
}
