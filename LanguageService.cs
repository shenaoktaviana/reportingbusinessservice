﻿using BusinessServiceFramework;
using BusinessServiceFramework.Models;
using ReportingDomainModel;
using ReportingDomainModel.Repositories;
using DomainModelFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace ReportingBusinessService
{
    public class LanguageService : BaseService<DomainRepository<Language>, Language>
    {
        public LanguageService()
            : base()
        {

        }

        public LanguageService(long userID)
            : base(userID)
        {

        }

        public override void AssignCollectionID(DataModel data, IQueryable<Language> filteredData)
        {
            data.CollectionID = filteredData.Select(x => (long)x.LanguageID).ToList();
        }

        protected override IQueryable<Language> Get(IQueryable<Language> data, string sort)
        {
            if (!string.IsNullOrEmpty(sort))
            {
                SortOption sortOption = (SortOption)Enum.Parse(typeof(SortOption), sort, true);

                if (sortOption == SortOption.NameAscending || sortOption == SortOption.NameDescending || sortOption == SortOption.Custom)
                {
                    switch (sortOption)
                    {
                        case SortOption.NameAscending:
                            data = data.OrderBy(o => o.Name);
                            break;
                        case SortOption.NameDescending:
                            data = data.OrderByDescending(o => o.Name);
                            break;
                        case SortOption.Custom:
                            data = data.OrderByDescending(o => o.IsDefault).ThenBy(o => o.Name);
                            break;
                    }
                }
                else
                {
                    data = base.Get(data, sort);
                }

            }

            return data;
        }

        public IEnumerable<Language> UpdateLanguages(IEnumerable<Language> models)
        {
            TransactionOptions transOpt = new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.RequiresNew, transOpt))
            {
                try
                {
                    foreach (Language model in models)
                    {
                        this.Update(model);
                        this.Save();
                    }

                    transaction.Complete();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return models;
        }

        public Language UpdateLanguage(Language model)
        {
            TransactionOptions transOpt = new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.RequiresNew, transOpt))
            {
                try
                {
                    this.Update(model);
                    this.Save();
                    transaction.Complete();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return model;
        }

        public Language InsertLanguage(Language model)
        {
            TransactionOptions transOpt = new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.RequiresNew, transOpt))
            {
                try
                {
                    this.Insert(model);
                    this.Save();
                    transaction.Complete();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return model;
        }

        public void DeleteLanguage(Language model)
        {
            TransactionOptions transOpt = new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.RequiresNew, transOpt))
            {
                try
                {
                    this.Delete(model);

                    this.Save();

                    transaction.Complete();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }
    }
}
