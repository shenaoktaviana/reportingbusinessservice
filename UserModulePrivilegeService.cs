﻿using BusinessServiceFramework;
using ReportingDomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelFramework.Repositories;
using ReportingDomainModel.Repositories;
using System.Linq.Expressions;
using BusinessServiceFramework.Models;
using LinqKit;

namespace ReportingBusinessService
{
    public class UserModulePrivilegeService : BaseService<DomainRepository<UserModulePrivilege>, UserModulePrivilege>
    {
        public UserModulePrivilegeService()
           : base()
        {

        }

        public UserModulePrivilegeService(long userID)
           : base(userID)
        {

        }
    }
}
