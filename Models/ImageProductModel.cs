﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CulinaryBizBusinessService.Models
{
    public class ImageProductModel
    {
        public string Path { get; set; }
        public string Variant1 { get; set; }
        public bool IsDefault { get; set; }
    }
}
