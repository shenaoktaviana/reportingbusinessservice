﻿using ReportingDomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReportingBusinessService;
using iTextSharp;

namespace ReportingBusinessService
{
    public class DetailReportModel
    {
        public DetailReportModel()
        {
        }

        public ReportRowType ReportRowType { get; set; }
        public ReportRowAlign RowHorizontalAlign { get; set; }
        public string Description { get; set; }
        public int Colspan { get; set; }
        public string Tag { get; set; }
        public ReportFontStyle FontStyle { get; set; }
    }
}