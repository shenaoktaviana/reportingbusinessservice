﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CulinaryBizBusinessService.Models
{
    public class ImageLanguageDetailPath
    {
        public long LanguageID { get; set; }
        public string MediumImagePath { get; set; }
        public string LargeImagePath { get; set; }
    }
}
