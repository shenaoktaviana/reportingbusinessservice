﻿using BusinessServiceFramework.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CulinaryBizBusinessService.Models
{
    public class UserSettings : BaseSettings
    {
        public int AdminPageSize { get; set; } = 10;

        [JsonIgnore]
        public long UserID { get; set; }
    }
}
