﻿using ReportingDomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReportingBusinessService;

namespace ReportingBusinessService
{
    public class HeaderReportModel
    {
        public HeaderReportModel()
        {

        }

        #region Header

        public string Type { get; set; }
        public string Title { get; set; }

        public long RootUserID { get; set; }
        public string RootUserName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string CompanyName { get; set; }
        public string CompanyLogo { get; set; }
        public string RootFilterName { get; set; }
        public string CurrentLoggedInName { get; set; }
        public string FooterDateFormat { get; set; }
        public float CompanyLogoHeight { get; set; }
        public float CompanyLogoWidth { get; set; }
        public bool IsLandscape { get; set; }

        public float[] ContentColumnWidths { get; set; }

        #endregion

        #region Detail

        public List<Tuple<string, ReportRowAlign>> HeaderColumns { get; set; }
        public List<DetailReportModel> DetailRows { get; set; }

        #endregion
    }
}