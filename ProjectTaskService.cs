﻿using BusinessServiceFramework;
using BusinessServiceFramework.Models;
using DomainModelFramework;
using LinqKit;
using ReportingDomainModel;
using ReportingDomainModel.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Transactions;

namespace ReportingBusinessService
{
    public class ProjectTaskService : BaseService<DomainRepository<ProjectTask>, ProjectTask>
    {
        public ProjectTaskService()
           : base()
        {

        }

        public ProjectTaskService(long userID)
           : base(userID)
        {

        }

        protected override IQueryable<ProjectTask> Get(IQueryable<ProjectTask> data, string sort)
        {
            if (!string.IsNullOrEmpty(sort))
            {
                SortOption sortOption = (SortOption)Enum.Parse(typeof(SortOption), sort, true);

                switch (sortOption)
                {
                    case SortOption.ProjectNameAscending:
                        data = data.OrderBy(o => o.Project.Name);
                        break;
                    case SortOption.ProjectNameDescending:
                        data = data.OrderByDescending(o => o.Project.Name);
                        break;
                    case SortOption.FirstInserted:
                        data = data.OrderBy(o => o.CreatedDate);
                        break;
                    case SortOption.LatestInserted:
                        data = data.OrderByDescending(o => o.CreatedDate);
                        break;
                    case SortOption.NameAscending:
                        data = data.OrderBy(o => o.Name);
                        break;
                    case SortOption.NameDescending:
                        data = data.OrderByDescending(o => o.Name);
                        break;
                    case SortOption.Oldest:
                    case SortOption.FirstUpdated:
                        data = data.OrderBy(o => o.ModifiedDate);
                        break;
                    case SortOption.Latest:
                    case SortOption.LatestUpdated:
                    default:
                        data = data.OrderByDescending(o => o.ModifiedDate);
                        break;
                }
            }

            return data;
        }

        protected override Expression<Func<ProjectTask, bool>> GetCustomFilterPredicate(FilterOption filter)
        {
            var predicate = PredicateBuilder.New<ProjectTask>(false);
            string term = filter.Value.ToString();

            switch (filter.Label.FirstOrDefault())
            {
                case "TaskName":
                    predicate = predicate.Or(x => (x.IsRoutine ? "(Routine) " + x.Name : x.Name).Contains(term));
                    break;

                case "ProjectName":
                    predicate = predicate.Or(x => x.Project.Name.Contains(term));
                    break;

                case "AssigneeName":
                    predicate = predicate.Or(x => x.ProjectTaskMemberDetails.Count(o => o.ProjectTaskMember.User.FirstName.ToLower().Contains(term.ToLower()) ||
                    o.ProjectTaskMember.User.LastName.ToLower().Contains(term.ToLower())) > 0);
                    break;

                case "ParentName":
                    predicate = predicate.Or(x => x.ParentProjectTask != null ? x.ParentProjectTask.Name.Contains(term) : false);
                    break;

                case "NoChilds":
                    predicate = predicate.Or(x => x.ChildProjectTasks.Count() == 0);
                    break;

                case "TaskNameAndDescription":
                    predicate = predicate.Or(
                        x => x.Name.ToLower().Contains(term.ToLower()) ||
                        x.ProjectTaskMemberDetails.Count(o => o.Description != null ? o.Description.ToLower().Contains(term.ToLower()) : false) > 0);
                    break;

            }

            return predicate;
        }

        public ProjectTask InsertProjectTask(ProjectTask model)
        {
            TransactionOptions transOpt = new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.RequiresNew, transOpt))
            {
                try
                {
                    this.Insert(model);

                    this.Save();

                    transaction.Complete();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            return model;
        }

        public ProjectTask UpdateProjectTask(ProjectTask model)
        {
            TransactionOptions transOpt = new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.RequiresNew, transOpt))
            {
                try
                {
                    this.Update(model);

                    this.Save();

                    transaction.Complete();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return model;
        }

        public IEnumerable<ProjectTask> UpdateProjectTasks(IEnumerable<ProjectTask> models)
        {
            TransactionOptions transOpt = new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.RequiresNew, transOpt))
            {
                try
                {
                    foreach (ProjectTask model in models)
                    {
                        this.Update(model);
                        this.Save();
                    }

                    transaction.Complete();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            return models;
        }

        public void DeleteProjectTask(ProjectTask model)
        {
            TransactionOptions transOpt = new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.RequiresNew, transOpt))
            {
                try
                {
                    this.Delete(model);

                    this.Save();

                    transaction.Complete();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }
    }
}
