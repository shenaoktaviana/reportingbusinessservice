﻿using BusinessServiceFramework;
using BusinessServiceFramework.Models;
using ReportingBusinessService.Models;
using DomainModelFramework;
using System.Net.Mail;

namespace ReportingBusinessService
{
    public class Mailer : BaseMailer
    {
        protected DeveloperSettings developerSettings;
        protected long userID;

        public Mailer(SMTPServerType type)
            : base(type)
        {
        }

        public Mailer(SMTPServerType type, DeveloperSettings developerSettings, long userID)
            : base(type)
        {
            this.developerSettings = developerSettings;
            this.userID = userID;
        }

        protected override void EmbedLogo(MailMessage message, string body, BaseInfo storeBaseInfo)
        {

        }

        protected override string GetMailHeaderImages(string absoluteURL, string body, BaseInfo storeBaseInfo)
        {
            return absoluteURL + "Images/logo.png";
        }

        protected override string GetMailFooterImages(string absoluteURL, string body, BaseInfo storeBaseInfo)
        {
            return absoluteURL + "Images/" + storeBaseInfo.Theme + "/email_footer.png";
        }

        protected override void OnSendingEmail(int cnt)
        {
            base.OnSendingEmail(cnt);
        }

        protected override void CreateLog(LogPriority logPriority, string title, string value, string entityType = null, long? entityID = null)
        {
            LogService logService = new LogService(this.userID, this.developerSettings, logPriority);
            logService.InsertLog(logPriority, title, value, entityType, entityID);
        }
    }
}
