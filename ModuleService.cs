﻿using BusinessServiceFramework;
using ReportingDomainModel;
using ReportingDomainModel.Repositories;

namespace ReportingBusinessService
{
    public class ModuleService : BaseService<DomainRepository<Module>, Module>
    {
        public ModuleService()
           : base()
        {

        }

        public ModuleService(long userID)
           : base(userID)
        {

        }
    }
}
