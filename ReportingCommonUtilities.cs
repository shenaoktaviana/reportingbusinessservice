﻿using BusinessServiceFramework;
using BusinessServiceFramework.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.draw;
using ReportingDomainModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;
using ReportingResources;
using DomainModelFramework;

namespace ReportingBusinessService
{
    public class ReportingCommonUtilities
    {
        public static PdfPTable CompanyNameAndLogoTable { get; set; }
        public static PdfPTable ReportTitleAndPeriodTable { get; set; }
        public static PdfPTable HeaderColumnTable { get; set; }
        public static PdfPTable ReportFooterTable { get; set; }
        public static bool BreakPage { get; set; }

        public static string EncodeFilename(string fileName)
        {
            return fileName.Replace("/", "_").Replace("\\", "_")
                .Replace(" ", "_").Replace(":", "_").Replace("<", "_").Replace(">", "_")
                .Replace("\"", "_").Replace("|", "_").Replace("?", "_").Replace("*", "_");
        }

        public static string GetExportPDF(List<HeaderReportModel> reportModel, bool repeatHeaderPerPage = false)
        {
            string docPath = HostingEnvironment.MapPath("~/Documents/");
            string type = reportModel.FirstOrDefault().Type;
            string title = reportModel.FirstOrDefault().Title;
            bool isLandscape = reportModel.FirstOrDefault().IsLandscape;
            string pdfTitle = title + "-" + DateTime.Now.ToString("ddyyhhmmss") + ".pdf";
            string pdfPath = pdfTitle;

            List<string> imagePaths = new List<string>();
            string imagePath = string.Empty;

            int columnCount = reportModel.FirstOrDefault().HeaderColumns.Count();

            #region DeletePreviousDocs
            try
            {
                CommonUtilities.DeleteFiles(docPath, CommonUtilities.GetFilesName(docPath, title + "*"));
            }
            catch (Exception e)
            {

            }

            #endregion

            #region DefineBasicSettings

            CommonUtilities.CreateFolderIfNeeded(docPath);

            //create a file stream object representing the actual file and name it to whatever you want.
            FileStream fs = new FileStream(docPath + "\\" + pdfTitle, FileMode.Create);

            // Create an instance of the document class which represents the PDF document itself.
            Document document = new Document();

            if (isLandscape)
                document.SetPageSize(PageSize.A4.Rotate());
            else
                document.SetPageSize(PageSize.A4);

            document.SetMargins(25, 25, 40, 40);

            #endregion

            #region DefaultProperties

            string fontPath = HostingEnvironment.MapPath("~/fonts/hind-regular.otf");
            string boldFontPath = HostingEnvironment.MapPath("~/fonts/hind-bold.otf");

            decimal multiplier = decimal.Divide(54, 96);
            float boxWidth = 0.2f;
            float leading = 1.2f;

            BaseFont customFont = BaseFont.CreateFont(fontPath, BaseFont.CP1252, BaseFont.EMBEDDED);
            BaseFont customBoldFont = BaseFont.CreateFont(boldFontPath, BaseFont.CP1252, BaseFont.EMBEDDED);

            BaseColor baseColor = BaseColor.BLACK;

            BaseColor boxColor = new BaseColor(150, 10, 10);
            BaseColor borderColor = new BaseColor(0, 0, 0);
            BaseColor whiteColor = new BaseColor(255, 255, 255);
            BaseColor tableColor = new BaseColor(219, 219, 219);
            BaseColor subHeaderColor = new BaseColor(245, 245, 245);

            BaseColor redColor = new BaseColor(150, 10, 10);

            float topHeaderFontSize = (float)(13 * multiplier);
            Font topHeaderFont = new Font(customFont, topHeaderFontSize, Font.NORMAL, baseColor);
            Font topHeaderFontBold = new Font(customFont, topHeaderFontSize, Font.BOLD, baseColor);
            Font companyNameFont = new Font(customFont, topHeaderFontSize, Font.NORMAL, baseColor);

            float headerFontSize = (float)(12 * multiplier);
            Font headerFont = new Font(customFont, headerFontSize, Font.NORMAL, whiteColor);

            float subHeaderFontSize = (float)(12 * multiplier);
            Font subHeaderFont = new Font(customFont, subHeaderFontSize, Font.NORMAL, baseColor);
            Font subHeaderFontBold = new Font(customFont, subHeaderFontSize, Font.BOLD, baseColor);

            float titleFontSize = (float)(20 * multiplier);
            Font titleFont = new Font(customFont, titleFontSize, Font.BOLD, baseColor);

            float subTitleFontSize = (float)(10 * multiplier);
            BaseColor subTitleColor = new BaseColor(95, 95, 95);
            Font subTitleFont = new Font(customFont, subTitleFontSize, Font.NORMAL, subTitleColor);
            Font subTitleBoldFont = new Font(customFont, subTitleFontSize, Font.BOLD, baseColor);

            float contentFontSize = (float)(11 * multiplier);
            BaseColor contentColor = new BaseColor(90, 90, 90);
            Font contentFont = new Font(customFont, contentFontSize, Font.NORMAL, baseColor);

            Paragraph paragraph = new Paragraph();
            List list = new List(List.UNORDERED);
            Phrase phrase = new Phrase();
            Chunk glue = new Chunk(new VerticalPositionMark());
            PdfPTable basicTable = new PdfPTable(1);
            PdfPCell cell = new PdfPCell();

            #endregion

            float cellTopPadding = 5;
            float defaultSpacing = 10;

            decimal paddingMultiplier = decimal.Divide(70, 100);
            float varNum = (float)(10 * paddingMultiplier);

            #region OpenDocument

            HeaderReportModel defaultReport = reportModel.FirstOrDefault();

            PdfWriter writer = PdfWriter.GetInstance(document, fs);

            writer.PageEvent = new CustomITextEvents(type, null, new iTextSharp.text.Font(customFont, contentFontSize, iTextSharp.text.Font.NORMAL, baseColor),
                defaultReport.CurrentLoggedInName, defaultReport.FooterDateFormat);

            //DefaultReportHeaderTableBeforeDataGenerated
            GenerateReportHeaderTable(title, defaultReport, companyNameFont, titleFont, topHeaderFont, defaultSpacing);
            GenerateReportHeaderColumn(columnCount, defaultSpacing, defaultReport, headerFont, boxColor, borderColor, boxWidth, cellTopPadding, leading);

            // Add meta information to the document
            document.AddKeywords(type);
            document.AddSubject(title);
            document.AddTitle(title);

            // Open the document to enable you to write to the document

            document.Open();

            #endregion

            string baseOrderImagePath = HostingEnvironment.MapPath("~/images");
            int index = 1;

            foreach (HeaderReportModel headerReportModel in reportModel)
            {
                if (BreakPage)
                {
                    GenerateReportHeaderTable(title, headerReportModel, companyNameFont, titleFont, topHeaderFont, defaultSpacing);
                    document.Add(CompanyNameAndLogoTable);
                    document.Add(ReportTitleAndPeriodTable);

                    GenerateReportHeaderColumn(columnCount, defaultSpacing, defaultReport, headerFont, boxColor, borderColor, boxWidth, cellTopPadding, leading);
                    document.Add(HeaderColumnTable);

                    BreakPage = false;
                }

                #region Data

                basicTable = new PdfPTable(columnCount);
                basicTable.WidthPercentage = 90;
                basicTable.HorizontalAlignment = Element.ALIGN_CENTER;

                if (headerReportModel.ContentColumnWidths != null && headerReportModel.ContentColumnWidths.Count() > 0)
                    basicTable.SetWidths(headerReportModel.ContentColumnWidths);

                foreach (DetailReportModel detailRow in headerReportModel.DetailRows)
                {
                    phrase = new Phrase();

                    bool allowSetCustomFont = true;

                    switch (detailRow.ReportRowType)
                    {
                        case ReportRowType.Content:
                            phrase.Font = contentFont;
                            break;
                        case ReportRowType.ParentGroupingName:
                            float groupParentFontSize = (float)(14 * multiplier);
                            Font groupParentFont = new Font(customFont, groupParentFontSize, Font.NORMAL, baseColor);

                            phrase.Font = groupParentFont;
                            break;
                        case ReportRowType.GroupingFooter:
                        case ReportRowType.GroupingName:

                            float groupFontSize = (float)(13 * multiplier);
                            Font groupFont = new Font(customFont, groupFontSize, Font.NORMAL, baseColor);

                            phrase.Font = groupFont;
                            break;
                        case ReportRowType.SubgroupName:

                            float subGroupFontSize = (float)(11 * multiplier);
                            Font subGroupFont = new Font(customFont, subGroupFontSize, Font.NORMAL, baseColor);

                            phrase.Font = subGroupFont;
                            break;
                        case ReportRowType.TableFooter:
                            phrase.Font = subHeaderFontBold;
                            break;
                    }

                    Font customFontStyle = null;

                    if (detailRow.FontStyle == ReportFontStyle.ITALIC)
                        customFontStyle = new Font(customFont, contentFontSize, Font.ITALIC, baseColor);
                    else if (detailRow.FontStyle == ReportFontStyle.BOLD)
                        customFontStyle = new Font(customFont, contentFontSize, Font.BOLD, baseColor);
                    else if (detailRow.FontStyle == ReportFontStyle.GRAY)
                        customFontStyle = new Font(customFont, contentFontSize, Font.NORMAL, BaseColor.GRAY);
                    else if (detailRow.FontStyle == ReportFontStyle.REDBOLD)
                        customFontStyle = new Font(customFont, contentFontSize, Font.BOLD, redColor);
                    else if (detailRow.FontStyle == ReportFontStyle.RED)
                        customFontStyle = new Font(customFont, contentFontSize, Font.NORMAL, redColor);

                    if (customFontStyle != null && allowSetCustomFont)
                        phrase.Font = customFontStyle;

                    switch (detailRow.Description)
                    {
                        default:
                            phrase.Add(detailRow.Description);
                            break;
                    }

                    PdfPCell detailRowCell = new PdfPCell(phrase);
                    detailRowCell.HorizontalAlignment = GetElementAlignment(detailRow.RowHorizontalAlign);
                    detailRowCell.Colspan = detailRow.Colspan;
                    detailRowCell.BorderColor = borderColor;
                    detailRowCell.BorderWidth = boxWidth;
                    detailRowCell.BorderWidthBottom = 0;
                    detailRowCell.Padding = cellTopPadding;
                    detailRowCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailRowCell.SetLeading(0, leading);

                    switch (detailRow.ReportRowType)
                    {
                        case ReportRowType.ParentGroupingName:
                            detailRowCell.BackgroundColor = new BaseColor(195, 195, 195);
                            break;
                        case ReportRowType.GroupingName:
                            detailRowCell.BackgroundColor = new BaseColor(217, 217, 217);
                            break;
                        case ReportRowType.GroupingFooter:
                            detailRowCell.BackgroundColor = new BaseColor(200, 200, 200);
                            break;
                        case ReportRowType.TableFooter:
                            detailRowCell.BackgroundColor = new BaseColor(195, 195, 195);
                            break;
                        case ReportRowType.SubgroupName:
                            detailRowCell.BackgroundColor = new BaseColor(240, 240, 240);
                            break;
                    }

                    basicTable.AddCell(detailRowCell);
                }

                //Blank row
                phrase = new Phrase();
                phrase.Font = contentFont;
                phrase.Add("");

                cell = new PdfPCell(phrase);
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BorderWidth = boxWidth;
                cell.BorderColor = borderColor;
                cell.BorderWidthTop = 0;
                cell.Colspan = headerReportModel.HeaderColumns.Count();
                basicTable.AddCell(cell);

                #endregion

                document.Add(basicTable);

                if (reportModel.Count() > 1 && reportModel.Count() != index)
                {
                    BreakPage = true;
                    document.NewPage();
                }

                index++;
            }

            // Close the document
            document.Close();

            // Close the writer instance
            writer.Close();

            // Always close open filehandles explicity
            fs.Close();

            return pdfPath;
        }

        public static int GetElementAlignment(ReportRowAlign reportRowAlign)
        {
            switch (reportRowAlign)
            {
                case ReportRowAlign.Center:
                    return Element.ALIGN_CENTER;
                case ReportRowAlign.Right:
                    return Element.ALIGN_RIGHT;
                case ReportRowAlign.Left:
                default:
                    return Element.ALIGN_LEFT;
            }
        }

        public static void GenerateReportHeaderTable(string reportTitle,
            HeaderReportModel headerReportModel, Font companyNameFont, Font titleFont, Font topHeaderFont, float defaultSpacing)
        {
            if (!string.IsNullOrEmpty(headerReportModel.CompanyLogo))
                CompanyNameAndLogoTable = new PdfPTable(new float[] { 0.5f, 0.5f });
            else
                CompanyNameAndLogoTable = new PdfPTable(new float[] { 0.5f });

            CompanyNameAndLogoTable.WidthPercentage = 90;
            CompanyNameAndLogoTable.HorizontalAlignment = Element.ALIGN_CENTER;
            CompanyNameAndLogoTable.SpacingBefore = defaultSpacing;

            #region Header-CompanyLogoAndName

            if (!string.IsNullOrEmpty(headerReportModel.CompanyLogo))
            {
                string imagePath = HostingEnvironment.MapPath("~/Images/" + headerReportModel.CompanyLogo);

                Image image = iTextSharp.text.Image.GetInstance(new Uri(imagePath));
                image.ScaleAbsoluteHeight(headerReportModel.CompanyLogoHeight);
                image.ScaleAbsoluteWidth(headerReportModel.CompanyLogoWidth);

                PdfPCell compLogoCell = new PdfPCell(image, false);
                compLogoCell.BorderWidth = 0;
                compLogoCell.VerticalAlignment = Element.ALIGN_CENTER;
                CompanyNameAndLogoTable.AddCell(compLogoCell);
            }

            Phrase phrase = new Phrase();
            phrase.Font = companyNameFont;
            phrase.Add(headerReportModel.CompanyName);

            PdfPCell cell = new PdfPCell(phrase);
            cell.BorderWidth = 0;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            CompanyNameAndLogoTable.AddCell(cell);

            #endregion

            #region TitlePeriodAndReportTitle Table

            ReportTitleAndPeriodTable = new PdfPTable(1);
            ReportTitleAndPeriodTable.WidthPercentage = 90;
            ReportTitleAndPeriodTable.HorizontalAlignment = Element.ALIGN_CENTER;

            phrase = new Phrase();
            phrase.Font = titleFont;
            phrase.Add(reportTitle);

            cell = new PdfPCell(phrase);
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.Colspan = 1;
            cell.BorderWidth = 0;
            ReportTitleAndPeriodTable.AddCell(cell);

            string dateTimeString = GeneralResources.Period + ": ";

            if (headerReportModel.StartDate == null && headerReportModel.EndDate == null)
                dateTimeString += FrameworkResources.All.ToUpper();

            if (headerReportModel.StartDate != null)
                dateTimeString += ((DateTime)headerReportModel.StartDate).ToString("dd MMMM yyyy") + " ";
            if (headerReportModel.EndDate != null)
                dateTimeString += "to " + ((DateTime)headerReportModel.EndDate).ToString("dd MMMM yyyy");

            phrase = new Phrase();
            phrase.Font = topHeaderFont;
            phrase.Add(dateTimeString);

            cell = new PdfPCell(phrase);
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.Colspan = 1;
            cell.BorderWidth = 0;
            ReportTitleAndPeriodTable.AddCell(cell);

            #endregion

            #region Header-RootFilterName Phrase

            if (!string.IsNullOrEmpty(headerReportModel.RootFilterName))
            {
                phrase = new Phrase();
                phrase.Font = topHeaderFont;
                phrase.Add(headerReportModel.RootFilterName);

                cell = new PdfPCell(phrase);
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.Colspan = 1;
                cell.BorderWidth = 0;
                ReportTitleAndPeriodTable.AddCell(cell);
            }

            #endregion
        }

        public static void GenerateReportHeaderColumn(int columnCount, float defaultSpacing, HeaderReportModel headerReportModel,
            Font headerFont, BaseColor boxColor, BaseColor borderColor, float boxWidth, float cellTopPadding, float leading)
        {
            HeaderColumnTable = new PdfPTable(columnCount);
            HeaderColumnTable.WidthPercentage = 90;
            HeaderColumnTable.HorizontalAlignment = Element.ALIGN_CENTER;
            HeaderColumnTable.SpacingBefore = defaultSpacing;

            if (headerReportModel.ContentColumnWidths != null && headerReportModel.ContentColumnWidths.Count() > 0)
                HeaderColumnTable.SetWidths(headerReportModel.ContentColumnWidths);

            foreach (Tuple<string, ReportRowAlign> column in headerReportModel.HeaderColumns)
            {
                Phrase phrase = new Phrase();
                phrase.Font = headerFont;
                phrase.Add(column.Item1);

                PdfPCell columnCell = new PdfPCell(phrase);
                columnCell.HorizontalAlignment = GetElementAlignment(column.Item2);
                columnCell.Colspan = 1;
                columnCell.BorderColor = borderColor;
                columnCell.BorderWidth = boxWidth;
                columnCell.BorderWidthBottom = 0;
                columnCell.PaddingTop = cellTopPadding;
                columnCell.PaddingBottom = cellTopPadding;
                columnCell.PaddingLeft = cellTopPadding;
                columnCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                columnCell.BackgroundColor = boxColor;
                columnCell.SetLeading(0, leading);

                HeaderColumnTable.AddCell(columnCell);
            }
        }
    }
}