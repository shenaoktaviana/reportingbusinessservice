﻿using ReportingDomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReportingBusinessService;
using iTextSharp;

namespace ReportingBusinessService
{
    public class FilterReportModel
    {
        public FilterReportModel()
        {
        }

        public bool IsPrint { get; set; }
        public string Type { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public IEnumerable<long> FilteredUserIds { get; set; }
        public IEnumerable<long> FilteredJobRoleIds { get; set; }
        public IEnumerable<long> FilteredProjectIds { get; set; }
        public IEnumerable<long> FilteredProjectTaskIds { get; set; }

        public IEnumerable<User> FilteredUsers { get; set; }
        public IEnumerable<JobRole> FilteredJobRoles { get; set; }
        public IEnumerable<Project> FilteredProjects { get; set; }
        public IEnumerable<ProjectTask> FilteredProjectTasks { get; set; }
    }
}