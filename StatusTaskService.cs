﻿using BusinessServiceFramework;
using ReportingDomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelFramework.Repositories;
using ReportingDomainModel.Repositories;
using System.Transactions;
using BusinessServiceFramework.Models;
using System.Linq.Expressions;
using LinqKit;
using DomainModelFramework;

namespace ReportingBusinessService
{
    public class StatusTaskService : BaseService<DomainRepository<StatusTask>, StatusTask>
    {
        public StatusTaskService()
            : base()
        {

        }

        public StatusTaskService(long userID)
            : base(userID)
        {

        }

        public IEnumerable<StatusTask> GetAllActiveStatus()
        {
            return this.GetBy(x => x.IsActive).OrderBy(x => x.Name);
        }
    }
}
