﻿using BusinessServiceFramework;
using BusinessServiceFramework.Models;
using DomainModelFramework;
using LinqKit;
using ReportingDomainModel;
using ReportingDomainModel.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Transactions;

namespace ReportingBusinessService
{
    public class ProjectTaskMemberService : BaseService<ProjectTaskMemberRepository, ProjectTaskMember>
    {
        public ProjectTaskMemberService()
           : base()
        {

        }

        public ProjectTaskMemberService(long userID)
           : base(userID)
        {

        }

        protected override IQueryable<ProjectTaskMember> Get(IQueryable<ProjectTaskMember> data, string sort)
        {
            if (!string.IsNullOrEmpty(sort))
            {
                SortOption sortOption = (SortOption)Enum.Parse(typeof(SortOption), sort, true);

                switch (sortOption)
                {
                    case SortOption.FirstInserted:
                        data = data.OrderBy(o => o.CreatedDate);
                        break;
                    case SortOption.LatestInserted:
                        data = data.OrderByDescending(o => o.CreatedDate);
                        break;
                    case SortOption.Oldest:
                        data = data.OrderBy(o => o.Date);
                        break;
                    case SortOption.FirstUpdated:
                        data = data.OrderBy(o => o.ModifiedDate);
                        break;
                    case SortOption.Latest:
                        data = data.OrderByDescending(o => o.Date);
                        break;
                    case SortOption.LatestUpdated:
                    default:
                        data = data.OrderByDescending(o => o.ModifiedDate);
                        break;
                }
            }

            return data;
        }

        protected override Expression<Func<ProjectTaskMember, bool>> GetCustomFilterPredicate(FilterOption filter)
        {
            var predicate = PredicateBuilder.New<ProjectTaskMember>(false);
            string term = filter.Value.ToString();

            switch (filter.Label.FirstOrDefault())
            {
                case "ProjectTaskName":
                    predicate = predicate.Or(x => x.ProjectTaskMemberDetails.Count(o => o.ProjectTask.Name.ToLower().Contains(term.ToLower())) > 0);
                    break;

                case "Description":
                    predicate = predicate.Or(x => x.ProjectTaskMemberDetails.Count(o => o.Description.Contains(term)) > 0);
                    break;

                case "SelfAndChildAssigneeID":
                    predicate = predicate.Or(x => x.AssigneeID == this.UserID || x.User.ParentID == this.UserID);
                    break;
            }

            return predicate;
        }

        public ProjectTaskMember InsertProjectTaskMember(ProjectTaskMember model)
        {
            TransactionOptions transOpt = new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.RequiresNew, transOpt))
            {
                try
                {
                    model.Status = ProjectTaskMemberStatus.APPROVED.ToString();
                    model.ReviewedBy = this.UserID;

                    this.Insert(model);
                    this.Repository.ManageState(model);

                    this.Save();

                    transaction.Complete();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            return model;
        }

        public ProjectTaskMember UpdateProjectTaskMember(ProjectTaskMember model)
        {
            TransactionOptions transOpt = new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.RequiresNew, transOpt))
            {
                try
                {
                    model.Status = ProjectTaskMemberStatus.APPROVED.ToString();
                    model.ReviewedBy = this.UserID;

                    foreach (ProjectTaskMemberDetail detail in model.ProjectTaskMemberDetails)
                        detail.ProjectTaskMemberID = model.ProjectTaskMemberID;

                    //this.Update(model);
                    model.ModifiedBy = this.UserID;
                    model.ModifiedDate = DateTime.Now;

                    this.Repository.ManageState(model);
                    this.Repository.Update(model);

                    this.Save();

                    transaction.Complete();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return model;
        }

        public IEnumerable<ProjectTaskMember> UpdateProjectTaskMembers(IEnumerable<ProjectTaskMember> models)
        {
            TransactionOptions transOpt = new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.RequiresNew, transOpt))
            {
                try
                {
                    foreach (ProjectTaskMember model in models)
                    {
                        this.Update(model);
                        this.Save();
                    }

                    transaction.Complete();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            return models;
        }

        public void DeleteProjectTaskMember(ProjectTaskMember model)
        {
            TransactionOptions transOpt = new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.RequiresNew, transOpt))
            {
                try
                {
                    this.Repository.DeleteDetails(model);
                    this.Delete(model);

                    this.Save();

                    transaction.Complete();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }
    }
}
