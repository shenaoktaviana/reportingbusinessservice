﻿using BusinessServiceFramework;
using ReportingDomainModel;
using ReportingDomainModel.Repositories;
using System;
using System.Collections.Generic;
using System.Transactions;
using System.Linq;
using System.Linq.Expressions;
using LinqKit;
using BusinessServiceFramework.Models;

namespace ReportingBusinessService
{
    public class ProjectTaskMemberDetailService : BaseService<DomainRepository<ProjectTaskMemberDetail>, ProjectTaskMemberDetail>
    {
        public ProjectTaskMemberDetailService()
           : base()
        {

        }

        public ProjectTaskMemberDetailService(long userID)
           : base(userID)
        {

        }

        protected override Expression<Func<ProjectTaskMemberDetail, bool>> GetCustomFilterPredicate(FilterOption filter)
        {
            var predicate = PredicateBuilder.New<ProjectTaskMemberDetail>(false);
            string term = filter.Value.ToString();

            switch (filter.Label.FirstOrDefault())
            {
                case "DescriptionContains":
                    predicate = predicate.Or(x => x.Description != null && x.Description.ToLower().Trim().Contains(term.ToLower().Trim()));
                    break;
            }

            return predicate;
        }

        public IEnumerable<ProjectTaskMemberDetail> GetAllTaskMemberDetailsByStatus(ProjectTaskMemberStatus status)
        {
            return this.GetBy(x => x.ProjectTaskMember.Status == status.ToString());
        }

        public IEnumerable<ProjectTaskMemberDetail> GetAllTaskMemberDetailsByStatusInPeriod(ProjectTaskMemberStatus status, DateTime startDate, DateTime endDate)
        {
            return this.GetAllTaskMemberDetailsByStatus(status).Where(x => x.ProjectTaskMember.Date >= startDate && x.ProjectTaskMember.Date <= endDate);
        }

        public ProjectTaskMemberDetail InsertProjectTaskMemberDetail(ProjectTaskMemberDetail model)
        {
            TransactionOptions transOpt = new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.RequiresNew, transOpt))
            {
                try
                {
                    this.Insert(model);

                    this.Save();

                    transaction.Complete();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            return model;
        }

        public ProjectTaskMemberDetail UpdateProjectTaskMemberDetail(ProjectTaskMemberDetail model)
        {
            TransactionOptions transOpt = new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.RequiresNew, transOpt))
            {
                try
                {
                    this.Update(model);

                    this.Save();

                    transaction.Complete();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return model;
        }

        public IEnumerable<ProjectTaskMemberDetail> UpdateProjectTaskMemberDetails(IEnumerable<ProjectTaskMemberDetail> models)
        {
            TransactionOptions transOpt = new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.RequiresNew, transOpt))
            {
                try
                {
                    foreach (ProjectTaskMemberDetail model in models)
                    {
                        this.Update(model);
                        this.Save();
                    }

                    transaction.Complete();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            return models;
        }

        public void DeleteProjectTaskMemberDetail(ProjectTaskMemberDetail model)
        {
            TransactionOptions transOpt = new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.RequiresNew, transOpt))
            {
                try
                {
                    this.Delete(model);

                    this.Save();

                    transaction.Complete();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }
    }
}
