﻿using BusinessServiceFramework;
using ReportingDomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelFramework.Repositories;
using ReportingDomainModel.Repositories;
using System.Transactions;
using BusinessServiceFramework.Models;
using System.Linq.Expressions;
using LinqKit;
using DomainModelFramework;

namespace ReportingBusinessService
{
    public class UserService : BaseService<UserRepository, User>
    {
        public UserService()
            : base()
        {

        }

        public UserService(long userID)
            : base(userID)
        {

        }

        public override void AssignCollectionID(DataModel data, IQueryable<User> filteredData)
        {
            data.CollectionID = filteredData.Select(x => x.UserID).ToList();
        }

        public User ValidateUserPassword(string email, string password)
        {
            User user = this.GetBy(x => x.Email == email).FirstOrDefault();

            if (user != null)
            {
                if (BCrypt.Net.BCrypt.Verify(password, user.Password))
                {
                    return user;
                }
            }

            return null;
        }

        public User UpdateUser(User model, bool changePassword, bool updatePrivileges = false, string password = null)
        {
            TransactionOptions transOpt = new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.RequiresNew, transOpt))
            {
                try
                {
                    if (changePassword)
                    {
                        if (!string.IsNullOrEmpty(password))
                        {
                            password = CommonUtilities.GenerateHashPassword(password);
                        }
                    }
                    else
                    {
                        password = null;
                    }

                    this.Repository.UpdateUser(model, this.UserID, changePassword, password, updatePrivileges);
                    this.Save();
                    transaction.Complete();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return model;
        }

        protected override IQueryable<User> Get(IQueryable<User> data, string sort)
        {
            if (!string.IsNullOrEmpty(sort))
            {
                SortOption sortOption = (SortOption)Enum.Parse(typeof(SortOption), sort, true);

                if (sortOption == SortOption.NameAscending || sortOption == SortOption.NameDescending)
                {
                    switch (sortOption)
                    {
                        case SortOption.NameAscending:
                            data = data.OrderBy(o => o.FirstName);
                            break;
                        case SortOption.NameDescending:
                            data = data.OrderByDescending(o => o.FirstName);
                            break;
                    }
                }
                else
                {
                    data = base.Get(data, sort);
                }

            }

            return data;
        }

        protected override Expression<Func<User, bool>> GetCustomFilterPredicate(FilterOption filter)
        {
            var predicate = PredicateBuilder.New<User>(false);

            switch (filter.Label.FirstOrDefault())
            {
                case "FullName":
                    string name = filter.Value.ToString();
                    predicate = predicate.Or(x => x.FirstName.ToLower().Contains(name.ToLower()) || x.LastName.ToLower().Contains(name.ToLower()));
                    break;
                case "Phone":
                    string phone = filter.Value.ToString();
                    predicate = predicate.Or(x => x.Phone.ToLower().Contains(phone.ToLower()));
                    break;
            }

            return predicate;
        }

        public IEnumerable<User> UpdateUsers(IEnumerable<User> models)
        {
            TransactionOptions transOpt = new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.RequiresNew, transOpt))
            {
                try
                {
                    foreach (User model in models)
                    {
                        this.Update(model);
                        this.Save();
                    }

                    transaction.Complete();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return models;
        }

        public User InsertUser(User model)
        {
            TransactionOptions transOpt = new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.RequiresNew, transOpt))
            {
                try
                {
                    model.Password = CommonUtilities.GenerateHashPassword(model.Password);

                    this.Insert(model);

                    if (model.UserModulePrivileges != null)
                    {
                        foreach (UserModulePrivilege userModulePrivilege in model.UserModulePrivileges)
                        {
                            userModulePrivilege.CreatedBy = this.UserID;
                            userModulePrivilege.ModifiedBy = this.UserID;
                            userModulePrivilege.ModifiedDate = DateTime.Now;
                            userModulePrivilege.CreatedDate = DateTime.Now;
                        }
                    }

                    this.Save();

                    transaction.Complete();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return model;
        }


        public void DeleteUser(User model)
        {
            TransactionOptions transOpt = new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.RequiresNew, transOpt))
            {
                try
                {
                    List<UserModulePrivilege> modelPrivilege = new List<UserModulePrivilege>();

                    foreach (UserModulePrivilege userModPriv in model.UserModulePrivileges)
                    {
                        modelPrivilege.Add(userModPriv);
                    }

                    this.Delete(model);
                    this.Repository.DeleteModulePrivileges(modelPrivilege);
                    this.Save();

                    transaction.Complete();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }


        public User ValidatePassword(string email, string password)
        {
            User user = this.GetBy(o => o.Email == email).FirstOrDefault();

            if (user != null && BCrypt.Net.BCrypt.Verify(password, user.Password))
                return user;

            return null;
        }
    }
}
