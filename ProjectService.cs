﻿using BusinessServiceFramework;
using ReportingDomainModel;
using ReportingDomainModel.Repositories;
using System;
using System.Collections.Generic;
using System.Transactions;

namespace ReportingBusinessService
{
    public class ProjectService : BaseService<DomainRepository<Project>, Project>
    {
        public ProjectService()
           : base()
        {

        }

        public ProjectService(long userID)
           : base(userID)
        {

        }

        public Project InsertProject(Project model)
        {
            TransactionOptions transOpt = new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.RequiresNew, transOpt))
            {
                try
                {
                    this.Insert(model);

                    this.Save();

                    transaction.Complete();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            return model;
        }

        public Project UpdateProject(Project model)
        {
            TransactionOptions transOpt = new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.RequiresNew, transOpt))
            {
                try
                {
                    this.Update(model);

                    this.Save();

                    transaction.Complete();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return model;
        }

        public IEnumerable<Project> UpdateProjects(IEnumerable<Project> models)
        {
            TransactionOptions transOpt = new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.RequiresNew, transOpt))
            {
                try
                {
                    foreach (Project model in models)
                    {
                        this.Update(model);
                        this.Save();
                    }

                    transaction.Complete();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            return models;
        }

        public void DeleteProject(Project model)
        {
            TransactionOptions transOpt = new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.RequiresNew, transOpt))
            {
                try
                {
                    this.Delete(model);

                    this.Save();

                    transaction.Complete();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }
    }
}
