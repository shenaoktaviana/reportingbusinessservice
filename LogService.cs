﻿using BusinessServiceFramework;
using BusinessServiceFramework.Models;
using ReportingBusinessService.Models;
using ReportingDomainModel;
using ReportingDomainModel.Repositories;
using DomainModelFramework;
using System;
using System.Transactions;

namespace ReportingBusinessService
{
    public class LogService : BaseService<DomainRepository<Log>, Log>
    {
        public bool _enableLogging = true;

        public LogService()
            : base()
        {
        }

        public LogService(long userID, DeveloperSettings developerSettings, LogPriority logPriority)
            : base(userID)
        {
            if (developerSettings != null)
            {
                this._enableLogging = developerSettings.EnableLogging
                    && (logPriority == LogPriority.HIGH ? developerSettings.EnableHighPriorityLogging : developerSettings.EnableLowPriorityLogging);
            }
        }

        public void InsertLog(LogPriority logPriority, string title, string value, string entityType = null, long? entityID = null)
        {
            TransactionOptions transOpt = new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.RequiresNew, transOpt))
            {
                if (this._enableLogging)
                {
                    try
                    {
                        Log log = new Log();

                        log.EntityType = entityType;
                        log.EntityID = entityID;
                        log.Title = title;
                        log.Value = value;

                        this.Insert(log);
                        this.Save();

                        transaction.Complete();
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                }
            }
        }
    }
}
