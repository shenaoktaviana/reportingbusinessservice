﻿using BusinessServiceFramework;
using ReportingDomainModel;
using ReportingDomainModel.Repositories;

namespace ReportingBusinessService
{
    public class CityService : BaseService<DomainRepository<City>, City>
    {
        public CityService()
           : base()
        {

        }

        public CityService(long userID)
           : base(userID)
        {

        }
    }
}
