﻿using BusinessServiceFramework;
using ReportingDomainModel;
using ReportingDomainModel.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace ReportingBusinessService
{
    public class JobRoleService : BaseService<DomainRepository<JobRole>, JobRole>
    {
        public JobRoleService()
           : base()
        {

        }

        public JobRoleService(long userID)
           : base(userID)
        {

        }

        public IEnumerable<JobRole> GetAllActiveJobRoles()
        {
            return this.GetBy(x => x.IsActive).OrderBy(x => x.Name);
        }

        public JobRole InsertJobRole(JobRole model)
        {
            TransactionOptions transOpt = new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.RequiresNew, transOpt))
            {
                try
                {
                    this.Insert(model);

                    this.Save();

                    transaction.Complete();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            return model;
        }

        public JobRole UpdateJobRole(JobRole model)
        {
            TransactionOptions transOpt = new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.RequiresNew, transOpt))
            {
                try
                {
                    this.Update(model);

                    this.Save();

                    transaction.Complete();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return model;
        }

        public IEnumerable<JobRole> UpdateJobRoles(IEnumerable<JobRole> models)
        {
            TransactionOptions transOpt = new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.RequiresNew, transOpt))
            {
                try
                {
                    foreach (JobRole model in models)
                    {
                        this.Update(model);
                        this.Save();
                    }

                    transaction.Complete();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            return models;
        }

        public void DeleteJobRole(JobRole model)
        {
            TransactionOptions transOpt = new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.RequiresNew, transOpt))
            {
                try
                {
                    this.Delete(model);

                    this.Save();

                    transaction.Complete();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }
    }
}
