﻿using BusinessServiceFramework;
using BusinessServiceFramework.Models;
using DomainModelFramework;
using LinqKit;
using ReportingDomainModel;
using ReportingDomainModel.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Transactions;

namespace ReportingBusinessService
{
    public class DailyScrumService : BaseService<DailyScrumRepository, DailyScrum>
    {
        public DailyScrumService()
           : base()
        {

        }

        public DailyScrumService(long userID)
           : base(userID)
        {

        }

        public DailyScrum InsertDailyScrum(DailyScrum model)
        {
            TransactionOptions transOpt = new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.RequiresNew, transOpt))
            {
                try
                {
                    this.Insert(model);
                    this.Repository.ManageState(model);

                    this.Save();

                    transaction.Complete();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            return model;
        }

        public DailyScrum UpdateDailyScrum(DailyScrum model)
        {
            TransactionOptions transOpt = new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.RequiresNew, transOpt))
            {
                try
                {
                    foreach (DailyScrumDetail detail in model.DailyScrumDetails)
                        detail.DailyScrumID = model.DailyScrumID;

                    //this.Update(model);
                    model.ModifiedBy = this.UserID;
                    model.ModifiedDate = DateTime.Now;

                    this.Repository.ManageState(model);
                    this.Repository.Update(model);

                    this.Save();

                    transaction.Complete();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return model;
        }

        public void DeleteDailyScrum(DailyScrum model)
        {
            TransactionOptions transOpt = new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };

            using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.RequiresNew, transOpt))
            {
                try
                {
                    this.Repository.DeleteDetails(model);
                    this.Delete(model);

                    this.Save();

                    transaction.Complete();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }
    }
}
