﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using BusinessServiceFramework;

namespace ReportingBusinessService
{
    public class CustomITextEvents : ITextEvents
    {
        public string UserName { get; set; }
        public string DateFormat { get; set; }

        public CustomITextEvents(string type, object storeInfo, Font font, string userName, string dateFormat)
            : base(type, storeInfo, font, true)
        {
            this.UserName = userName;
            this.DateFormat = string.IsNullOrEmpty(dateFormat) ? "" : dateFormat;
        }

        public override void OnStartPage(PdfWriter writer, Document document)
        {
            base.OnStartPage(writer, document);

            if (!ReportingCommonUtilities.BreakPage)
            {
                ReportingCommonUtilities.BreakPage = false;

                if (ReportingCommonUtilities.CompanyNameAndLogoTable != null)
                    document.Add(ReportingCommonUtilities.CompanyNameAndLogoTable);

                if (ReportingCommonUtilities.ReportTitleAndPeriodTable != null)
                    document.Add(ReportingCommonUtilities.ReportTitleAndPeriodTable);

                if (ReportingCommonUtilities.HeaderColumnTable != null)
                    document.Add(ReportingCommonUtilities.HeaderColumnTable);
            }
        }

        public override void OnEndPage(PdfWriter writer, Document document)
        {
            base.OnEndPage(writer, document);

            String page = writer.PageNumber.ToString();
            float len = this.contentFont.GetWidthPoint(page, 8);

            //Add username to footer
            if (!string.IsNullOrEmpty(this.UserName))
            {
                this.contentByte.BeginText();
                this.contentByte.SetFontAndSize(this.contentFont, 8);
                this.contentByte.SetTextMatrix(document.PageSize.GetLeft(50), document.PageSize.GetBottom(15));
                this.contentByte.ShowText(this.UserName);
                this.contentByte.EndText();
                len = this.contentFont.GetWidthPoint(this.UserName, 8);
                this.contentByte.AddTemplate(this.footerTemplate, document.PageSize.GetRight(90) + len, document.PageSize.GetBottom(15));
            }

            //Add paging to footer
            this.contentByte.BeginText();
            this.contentByte.SetFontAndSize(this.contentFont, 8);
            this.contentByte.SetTextMatrix(document.PageSize.GetLeft(280), document.PageSize.GetBottom(15));
            this.contentByte.ShowText(page);
            this.contentByte.EndText();
            len = this.contentFont.GetWidthPoint(page, 8);
            this.contentByte.AddTemplate(this.footerTemplate, document.PageSize.GetRight(90) + len, document.PageSize.GetBottom(15));

            //Add datetime to footer
            this.contentByte.BeginText();
            this.contentByte.SetFontAndSize(this.contentFont, 8);
            this.contentByte.SetTextMatrix(document.PageSize.GetRight(115), document.PageSize.GetBottom(15));
            this.contentByte.ShowText(DateTime.Now.ToString(this.DateFormat));
            this.contentByte.EndText();
            len = this.contentFont.GetWidthPoint(DateTime.Now.ToString(this.DateFormat), 8);
            this.contentByte.AddTemplate(this.footerTemplate, document.PageSize.GetRight(90) + len, document.PageSize.GetBottom(15));

        }

        public override void OnOpenDocument(PdfWriter writer, Document document)
        {
            base.OnOpenDocument(writer, document);
        }
    }
}