﻿using BusinessServiceFramework;
using ReportingDomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelFramework.Repositories;
using ReportingDomainModel.Repositories;
using System.Linq.Expressions;
using BusinessServiceFramework.Models;
using LinqKit;

namespace ReportingBusinessService
{
    public class StateService : BaseService<DomainRepository<State>, State>
    {
        public StateService()
           : base()
        {

        }

        public StateService(long userID)
           : base(userID)
        {

        }
    }
}
